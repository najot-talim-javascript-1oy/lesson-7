// ===============================================================
// String1. n butun soni berilgan (1 <= n <= 26). Lotin alfavitidagi dastlabki n ta katta harflarni chiqaruvchi programma tuzilsin.

// function kottaHarflar(n) {
//     if (n < 27){
//         for (let i = 0; i < n; i++) {
//           const kod = 65 + i; // 65 'A' kodini ifodalaydi
//           console.log(String.fromCharCode(kod));
//         }
//     }else{
//         console.log('Xatolik');
//     }
// }
// kottaHarflar(26); 


// String2. Kiritilgan belgining nimaligini aniqlovchi programma tuzilsin. Agar kiritilgan belgi raqam bo'lsa - "digit", lotincha harf bo'lsa - "lotin" yozuvi chiqarilsin. Boshqa xolatlar uchun nol chiqarilsin.

// function belgi_qanqa(belgi) {
//     const char = belgi.charCodeAt(0);
//     if (char >= 48 && char <= 57) {
//       return "digit";
//     } else if ((char >= 65 && char <= 90) || (char >= 97 && char <= 122)) {
//       return "lotin";
//     } else {
//       return "nol";
//     }
// }
// console.log(belgi_qanqa("1")); 
// console.log(belgi_qanqa("A")); 
// console.log(belgi_qanqa("#")); 


// String3. Satr beilgan. Satrning birinchi va oxirgi belgisini UTF-16 jadvalidagi kodini chiqaruvchi programma tuzilsin.

// function UTFKod(satr) {
//     const birBelgi = satr.charCodeAt(0);
//     const oxirgBelgi = satr.charCodeAt(satr.length - 1);
//     console.log("Birinchi belgi UTF-16 kodi: " + birBelgi);
//     console.log("Oxirgi belgi UTF-16 kodi: " + oxirgBelgi);
// }
// UTFKod("Salom"); 


// String4. N natural soni va belgi berilgan. N ta kiritilgan belgidan iborat satr hosil qiling va ekranga chiqaring. Masalan: N = 5; Belgi = 'A'; Natija = AAAAA

// function Satr(n, belgi) {
//     let satr = "";
//     for (let i = 0; i < n; i++) {
//       satr += belgi;
//     }
//     console.log(satr);
// }
// Satr(5, "A"); 

// String5. Kiritilgan satrni teskari tartibda chiqaruvchi programma tuzilsin. 

// function teskariTartib(satr) {
//     let result = "";
//     for (let i = satr.length - 1; i >= 0; i--) {
//       result += satr[i];
//     }    
//     console.log(result);
// }
// teskariTartib("Salom"); 

// String6. Satr va N natural soni berilgan. Shu satr belgilari orasiga N tadan “*” belgisi qo’yib yangi satr qaytaruvchi getStringStars(N) nomli funksiya tuzilsin.

// function getStringStars(n, satr) {
//     let result = "";
//     for (let i = 0; i < satr.length; i++) {
//       result += satr[i];   
//       if (i < satr.length - 1) {
//         for (let j = 0; j < n; j++) {
//           result += "*";
//         }
//       }
//     }
//     return result;
// }
// console.log(getStringStars(1, "Salom"));

// String7. Satr berilgan. Satrdagi raqamlar sonini aniqlovchi programma tuzilsin. (for of va isNaN dan foydalaning !)

// function sonAniqlovchi(satr) {
//     let num = "";
//     for (let harf of satr) {
//       if (!isNaN(harf)) {
//         num += harf;
//     }
//     }return Number(num);
// }
// console.log(sonniAniqlovchi("12345"));
  

// String8. Satr berilgan. Satrdagi kichik lotin va kirill harflarining umumiy sonini aniqlovchi programma tuzilsin.

// function harf(satr) {
//     let kirill = 0;
//     let lotin = 0;
//     for (let i = 0; i < satr.length; i++) {
//       let harf = satr[i];
//       let kod = harf.charCodeAt(0); 
//       if ((kod >= 65 && kod <= 90) || (kod >= 97 && kod <= 122)) {
//         lotin++;
//       } else if ((kod >= 1040 && kod <= 1103) || (kod >= 1072 && kod <= 1175)) {
//         kirill++;
//       }
//     }return { kirill, lotin };
// }
// console.log(harf ("Salom Дунё!")); 
  

// String9. Satr berilgan. Satrdagi xamma katta lotin harflari kichigiga almashtiruvchi programma tuzilsin.

// function Lotina(satr) {
//     let kichikS = "";
//     for (let i = 0; i < satr.length; i++) {
//       let harf = satr[i];
//       let kod = harf.charCodeAt(0);      
//       if (kod >= 65 && kod <= 90) {
//         kichikS += String.fromCharCode(kod + 32);
//       } else {
//         kichikS += harf;
//       }
//     }return kichikS;
// }
// console.log(Lotina("Salom Dunyo!"));
  

// String10. Satr berilgan. Satrdagi xamma katta harflarini kichigiga, kichiklarini kattasiga almashtiruvchi programma tuzilsin.

// function rv(satr) {
//     let result = "";
//     for (let i = 0; i < satr.length; i++) {
//       let harf = satr[i];
//       let kod = harf.charCodeAt(0); 
//       if (kod >= 65 && kod <= 90) {
//         result += String.fromCharCode(kod + 32); // katta harfn kichigiga almashtirish
//       } else if (kod >= 97 && kod <= 122) {
//         result += String.fromCharCode(kod - 32); // kichik harfn kattasiga almashtirish
//       } else {
//         result += harf;
//       }
//     }return result;
// }
// console.log(rv("Salom Dunyo!")); 
  

// String11. Satr berilgan. Agar satrda butun son ifodalangan bo'lsa 1 chiqarilsin, agar haqiqiy son bo'lsa 2 chiqarilsin. Agar satrni songa aylantirish imkoni bo'lmasa 0 chiqarilsin. Haqiqiy sonning kasr qismi nuqta "." Bilan ajratilgan deb qabul qilinsin.

// function Tu(str) {
//     const numRegex = /^-?\d+\.?\d*$/;
//     if (numRegex.test(str)) {
//       return str.includes('.') ? 2 : 1; // nuqta borligini tekshirish
//     }
//     if (str.split('').some((c) => isNaN(parseInt(c)))) {
//       return 0;
//     }
//     return 0;
// }
// console.log(Tu("123")); 
// console.log(Tu("-0.45")); 
  

// String12. Berilgan n sonni teskarisiga almashtiruvchi getInverseNumber(n) nomli funksiya tuzing.

// function getInverseNumber(n) {
//     let reversed = 0;
//     while (n > 0) {
//       let digit = n % 10;
//       reversed = reversed * 10 + digit;
//       n = Math.floor(n / 10);
//     }
//     return reverse;
// }
// console.log(getInverseNumber(1234)); 
// console.log(getInverseNumber(8675309));

// String13. "son ± son±... ± son" ko'rinishidagi arifmetik ifodani bildiruvchi satr berilgan. "+" belgisi o'rnida yo "+" yoki "-" bo'ladi. (Masalan: 7 + 3 - 2) Ifodaning qiymatini aniqlovchi programma tuzilsin. (son o\'rnida butun son kiritiladi)

// function evaluateExpression(expression) {
//     let result = 0;
//     let operator = "+";
//     let number = "";
//     for (let i = 0; i < expression.length; i++) {
//       const currentChar = expression[i];
//       if (!isNaN(parseInt(currentChar))) {
//         number += currentChar;
//       }
//       if (isNaN(parseInt(currentChar)) || i === expression.length - 1) {
//         const currentNumber = parseInt(number); 
//         if (operator === "+") {
//           result += currentNumber;
//         } else {
//           result -= currentNumber;
//         }    
//         operator = currentChar;
//         number = "";
//       }
//     }
//     return result;
// }
// const expression = "7+3-2";
// const result = evaluateExpression(expression);
// console.log(result);
  
  
// String14. N1, N2 natural sonlari va S1, S2 satr berilgan. S1 satrning dastlabki N1 ta belgisidan va S2 satrning oxirgi N2 ta belgisidan iborat yangi satr hosil qiling.

// function NwString(N1, N2, S1, S2) {
//     const fSubstring = S1.substring(N1);
//     const sSubstring = S2.substring(0, S2.length - N2);
//     const nString = fSubstring + sSubstring;
//     return newString;
// }
// const N1 = 3;
// const N2 = 4;
// const S1 = "Hello, world!";
// const S2 = "Goodbye, world!";
// const newString = NwString(N1, N2, S1, S2);
// console.log(newString);
  
// String15. C belgisi va S satri berilgan. S satrida uchragan har bir C belgisini 2 marta orttiruvchi programma tuzilsin.

// function Char(c, s) {
//     let result = "";
//     for (let i = 0; i < s.length; i++) {
//       if (s[i] === c) {
//         result += c + c;
//       } else {
//         result += s[i];
//       }
//     }
//     return result;
// }
// const C = 'a';
// const S = 'abacaba';
// const newS = Char(C, S);
// console.log(newS);
  
// String16. C belgisi va S1, S2 satrlari berilgan. S1 satriga shu satrda uchragan har bir C belgisidan oldin S2 satrini qo'shuvchi programma tuzilsin.

// function BeforeC(c, s1, s2) {
//     let result = "";
//     for (let i = 0; i < s1.length; i++) {
//       if (s1[i] === c) {
//         result += s2 + c;
//       } else {
//         result += s1[i];
//       }
//     }
//     return result;
//   }  
// const C = '';
// const S1 = 'abacaba';
// const S2 = '123';  
// const newS = BeforeC(C, S1, S2);
// console.log(newS);
  
// String17. S1 va S2 satrlari berilgan. S2 satrini S1 satrida takrorlanishlar sonini chiqaruvchi programma tuzilsin.

// function countStringOccurrences(s1, s2) {
//     let count = 0;
//     let index = 0;
//     while ((index = s1.indexOf(s2, index)) !== -1) {
//       count++;
//       index += s2.length;
//     }
//     return count;
// }  
// const S1 = 'abababa';
// const S2 = 'aba';
// const count = countStringOccurrences(S1, S2);
// console.log(count);
  
// String18. S1 va S2 satrlari berilgan. S1 satrida birinchi uchragan S2 satrini o'chirib tashlovchi programma tuzilsin. Agar S1 satrida S2 satri uchramasa S1 satri o'zgarishsiz qoldirilsin.

// function removeFirstOccurrence(s1, s2) {
//     const index = s1.indexOf(s2);
//     if (index !== -1) {
//       return s1.slice(0, index) + s1.slice(index + s2.length);
//     }
//     return s1;
// }
// const S1 = 'Hello world!';
// const S2 = 'lo';
// const result = removeFirstOccurrence(S1, S2);
// console.log(result);
  

// String19. S1 va S2 satrlari berilgan. S1 satrida uchragan barcha S2 satrlarini o'chirib tashlovchi programma tuzilsin. Agar S1 satrida S2 satri uchramasa S1 satri o'zgarishsiz qoldirilsin.

// function removeAllOccurrences(s1, s2) {
//     let result = s1;
//     let index = result.indexOf(s2);
//     while (index !== -1) {
//       result = result.slice(0, index) + result.slice(index + s2.length);
//       index = result.indexOf(s2);
//     }
//     return result;
// }  
// const S1 = 'Hello world, world!';
// const S2 = 'world';
  
// const result = removeAllOccurrences(S1, S2);
// console.log(result);
  
// String20. S1, S2 va S3 satrlari berilgan. S1 satrida birinchi uchragan S2 satrini S3 satriga o'zgartiruvchi programma tuzilsin.

// function replaceFirstOccurrence(s1, s2, s3) {
//     const index = s1.indexOf(s2);
//     if (index !== -1) {
//       return s1.slice(0, index) + s3 + s1.slice(index + s2.length);
//     }
//     return s1;
// }
// const S1 = 'Hello world, world!';
// const S2 = 'world';
// const S3 = 'planet';
// const result = replaceFirstOccurrence(S1, S2, S3);
// console.log(result);
  

// String21. S1, S2 va S3 satrlari berilgan. S1 satrida oxirgi uchragan S2 satrini S3 satriga o'zgartiruvchi programma tuzilsin.

// function replaceLastOccurrence(s1, s2, s3) {
//     const index = s1.lastIndexOf(s2);
//     if (index !== -1) {
//       return s1.slice(0, index) + s3 + s1.slice(index + s2.length);
//     }
//     return s1;
// }
// const S1 = 'Hello world, world!';
// const S2 = 'world';
// const S3 = 'planet';  
// const result = replaceLastOccurrence(S1, S2, S3);
// console.log(result);
  

// String22. Probel bilan ajratilgan va faqat katta harflar bilan terilgan o'zbekcha so'zlardan iborat satr berilgan. Satrdagi bir xil harflar bilan boshlanuvchi va tugovchi so'zlar sonini aniqlovchi programma tuzilsin.

// function findSameWordCount(str) {
//     const words = str.split(" ");
//     const firstLetters = {};
//     const lastLetters = {};
//     let count = 0;
  
//     for (let i = 0; i < words.length; i++) {
//       const word = words[i];
//       const firstLetter = word[0];
//       const lastLetter = word[word.length - 1];
      
//       if (firstLetter in firstLetters && lastLetter in lastLetters &&
//           firstLetters[firstLetter] === lastLetters[lastLetter]) {
//         count++;
//       } else {
//         firstLetters[firstLetter] = i;
//         lastLetters[lastLetter] = i;
//       }
//     }return count;
// }
// const str = "Assalomu alaykum dunyo do'stlari";
// const count = findSameWordCount(str);
// console.log(count); 
    

// String23. Probel bilan ajratilgan va faqat katta harflar bilan terilgan o'zbekcha so'zlardan iborat satr berilgan. Satrdagi aniq 2 ta 'A' harfi bor so'zlar sonini aniqlovchi programma tuzilsin.

// function countWordsWithTwoA(str) {
//     let count = 0;
//     const words = str.split(' ');
//     for (let i = 0; i < words.length; i++) {
//       const word = words[i];
//       if (word.indexOf('\u0041') !== -1 && word.lastIndexOf('\u0041') !== word.indexOf('\u0041')) {
//         count++;
//       }
//     }
//     return count;
//   }
//   const sentence = 'Alisher navbatda qaraytiRgan kattA deb o\'ylagAnlar bilAn uchrashdi';
//   console.log(countWordsWithTwoA(sentence)); // 2
  

// String24. Probel bilan ajratilgan o'zbekcha so'zlardan iborat str nomli satr berilgan. Satrdagi eng uzun so'zni qaytaruvchi getMaxOfString(str) nomli funksiya tuzilsin.

// function MaxProb(str) {
//     let tx = str.split(' ');
//     let maxLength = 0;
//     let longestWord = '';
//     for (let i = 0; i < tx.length; i++) {
//       let currentWordLength = tx[i].length;
//       if (currentWordLength > maxLength) {
//         maxLength = currentWordLength;
//         longestWord = tx[i];
//       }
//     } 
//     return longestWord;
// }
// let str = "Salom, dunyo! Bugun havo yaxshi. Sizni ko'rishdan.";
// console.log(MaxProb(str));
    


// String25. Probel bilan ajratilgan o'zbekcha so'zlardan iborat satr berilgan. Shu satrdagi so'zlarni “.” bilan ajratilgan xolda chiqaruvchi programma tuzilsin. Satr oxiriga "." qo'yish shart emas.

// function Prob(str) {
//     let tx = str.split(" ");
//     let result = tx.join(" . ");
//     return result;
// }
// let str = "Bu bir necha so'zdan iborat matn";
// console.log(Prob(str));
  

// String26. Probel bilan ajratilgan kichik harfli so'zlardan iborat satr berilgan. Satrdagi har bir so'zning birinchi harfini kattasi bilan almashtiruvchi programma tuzilsin.

// function KottaH_1(str) {
//     let tx = str.split(" ");
//     for (let i = 0; i < tx.length; i++) {
//       let word = tx[i];
//       tx[i] = word.charAt(0).toUpperCase() + word.slice(1);
//     }
//     return tx.join(" ");
// }
// let test = "bugun havo juda yaxshi";
// console.log(KottaH_1(test));
  


// String27. Satr berilgan. Satrdagi tinish belgilari sonini chiqaruvchi programma tuzilsin. marks = [".", ",", ";", ":", "?", "!", "-", "'", '"', "(", ")"]

// function countP(str) {
//     const marks = [".", ",", ";", ":", "?", "!", "-", "'", '"', "(", ")"];
//     let count = 0;
//     for (let i = 0; i < str.length; i++) {
//       if (marks.includes(str[i])) {
//         count++;
//       }
//     }
//     return count;
// }
// let str = "Bu mening. test, matn. Im 123 test matn! Va men buni testlayapman...";
// let count = countP(str);
// console.log(count);
    


// String28. Faylning to'liq nomini o'zida akslantirgan satr berilgan. Ya'ni disk nomi, papkalar nomi, faylning nomi va kengaytmasi. Satrdan faylning nomini (kengaytmasiz) aniqlovchi programma tuzilsin. Input: D:/Qudrat_c++/books/My_book.exe Output: My_book

// function Oxr_1(path) {
//     let Name = path.split('/').pop(); 
//     Name = Name.split('.').slice(0, -1).join('.'); 
//     return Name;
// }
// console.log(Oxr_1('D:/Qudrat_c++/books/My_book.exe'));
  

// String29. Faylning to'liq nomini o'zida akslantirgan satr berilgan. Ya'ni disk nomi, papkalar nomi, faylning nomi va kengaytmasi. Satrdan faylning kengaytmasini aniqlovchi programma tuzilsin. Input: D:/Qudrat_c++/books/My_book.exe Output: exe

// function ExeOxr(filePath) {
//     var dotI = filePath.lastIndexOf(".");
//     var ext = filePath.substring(dotI + 1);
//     return ext;
// }
// console.log(ExeOxr("D:/Qudrat_c++/books/My_book.exe"));


// String30. Faylning to'liq nomini o'zida akslantirgan satr berilgan. Ya'ni disk nomi, papkalar nomi, faylning nomi va kengaytmasi. Satrdan oxirgi papka nomini aniqlovchi programma tuzilsin. Agar papka tub bo'lsa (корневой), "/" belgisi chiqarilsin. Input: D:/Qudrat_c++/books/My_book.exe Output: books

// function Books(path) {
//     const Index = path.lastIndexOf("/");
//     if (Index === -1) { 
//       return ".";
//     } else if (Index === 0) {
//       return "/";
//     } else { 
//       const secondLastIndex = path.lastIndexOf("/", Index - 1);
//       return path.substring(secondLastIndex + 1, Index);
//     }
// }
// const path = "D:/Qudrat_c++/books/My_book.exe";
// const boo = Books(path);
// console.log(boo); 
    